package it.unimore.ing.homeplugs;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashSet;

public class HomePlugsDatabaseHandler extends SQLiteOpenHelper {
    // versione db
    private static final int DATABASE_VERSION = 1;
    //
    // nome db
    private static final String DATABASE_NAME = "HomePlugsDB";
    //
    // nomi tabelle
    private static final String TABLE_PLUGS = "plugs";
    private static final String TABLE_PROGRAMS = "programs";
    //
    // nomi colonne table plug
    private static final String KEY_ADDRESS = "mac";
    private static final String KEY_NAME = "name";
    private static final String KEY_STATE = "state";
    private static final String KEY_FAV = "favourite";
    private static final String KEY_ALARM = "alarm";
    // nomi colonne table program
    private static final String KEY_WEEK = "week";
    private static final String KEY_TIMEON = "timeon";
    private static final String KEY_TIMEOFF = "timeoff";
    private static final String KEY_ID = "pos";
    //
    private static HomePlugsDatabaseHandler ourInstance;

    private static Context myContext;

    private HomePlugsDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void setContext(Context context) {
        HomePlugsDatabaseHandler.myContext = context;
    }

    public static synchronized HomePlugsDatabaseHandler getInstance(Context context) {
        if (myContext == null) {
            myContext = context;
        }

        if (ourInstance == null) {
            ourInstance = new HomePlugsDatabaseHandler(myContext);
        }
        return ourInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*
        CREATE TABLE PLUGS (
	        mac VARCHAR PRIMARY KEY,
	        name VARCHAR,
	        state BOOLEAN,
	        favourite BOOLEAN,
	        alarm BOOLEAN )
        */
        String CREATE_PLUGS_TABLE =
                "CREATE TABLE " + TABLE_PLUGS +
                        " (" + KEY_ADDRESS + " TEXT PRIMARY KEY, " +
                        KEY_NAME + " TEXT, " +
                        KEY_STATE + " TEXT, " +
                        KEY_FAV + " INTEGER, " +
                        KEY_ALARM + " INTEGER)";
        sqLiteDatabase.execSQL(CREATE_PLUGS_TABLE);

        /*
        CREATE TABLE PROGRAMS (
	        pos INTEGER,
	        address VARCHAR REFERENCES PLUGS (mac),
	        timeon VARCHAR,
	        timeoff VARCHAR,
	        week VARCHAR,
	        PRIMARY KEY (pos,address)
	        )
        */
        String CREATE_PROGRAMS_TABLE =
                "CREATE TABLE " + TABLE_PROGRAMS +
                        " ( " + KEY_ID + " INTEGER, " +
                        KEY_ADDRESS + " TEXT REFERENCES " + TABLE_PLUGS + " (" + KEY_ADDRESS + ") ON DELETE CASCADE, " +
                        KEY_TIMEON + " TEXT, " +
                        KEY_TIMEOFF + " TEXT, " +
                        KEY_WEEK + " TEXT, " +
                        " PRIMARY KEY ( " + KEY_ID + " , " + KEY_ADDRESS + " ) )";
        sqLiteDatabase.execSQL(CREATE_PROGRAMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PLUGS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PROGRAMS);
        onCreate(sqLiteDatabase);
    }

    /// -------------- PLUGs --------------- ///

    // Adding new plug
    public void addPlug(Plug plug) {
        SQLiteDatabase HomePlugsDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ADDRESS, plug.getAddress());
        values.put(KEY_NAME, plug.getName());
        values.put(KEY_STATE, plug.getState());
        values.put(KEY_FAV, plug.getIntFav());
        values.put(KEY_ALARM, plug.getIntAlarm());

        HomePlugsDB.insert(TABLE_PLUGS, null, values);
        HomePlugsDB.close();
    }

    // Getting single plug
    public Plug getPlug(String address) {
        SQLiteDatabase HomePlugsDB = getReadableDatabase();
        Cursor cursor = HomePlugsDB.query(
                TABLE_PLUGS,
                new String[]{KEY_ADDRESS, KEY_NAME, KEY_STATE, KEY_FAV, KEY_ALARM},
                KEY_ADDRESS + "= ?", new String[]{address},
                null, null, null, null);

        if (cursor.getCount() == 1) {
            cursor.moveToFirst();

            Plug new_plug = new Plug(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    Integer.valueOf(cursor.getString(3)),
                    Integer.valueOf(cursor.getString(4)));

            return new_plug;
        }
        return null;
    }

    // Getting All Plugs
    public HashSet<Plug> getAllPlugs() {
        HashSet<Plug> internal_hashset = new HashSet<>();
        SQLiteDatabase HomePlugsDB = getReadableDatabase();

        Cursor cursor = HomePlugsDB.rawQuery("SELECT * FROM " + TABLE_PLUGS + " ORDER BY " + KEY_ADDRESS, null);

        if (cursor.moveToFirst()) {
            do {
                Plug new_plug = new Plug(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        Integer.valueOf(cursor.getString(3)),
                        Integer.valueOf(cursor.getString(4)));
                internal_hashset.add(new_plug);
            } while (cursor.moveToNext());
        }
        return internal_hashset;
    }

    // Updating single plug
    public void updatePlug(Plug plug) {
        SQLiteDatabase HomePlugsDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, plug.getName());
        values.put(KEY_STATE, plug.getState());
        values.put(KEY_FAV, plug.getFav());
        values.put(KEY_ALARM, plug.getAlarm());
        HomePlugsDB.update(TABLE_PLUGS, values, KEY_ADDRESS + " = ?", new String[]{plug.getAddress()});
        HomePlugsDB.close();
    }

    public void deletePlug(Plug plug) {
        SQLiteDatabase HomePlugsDB = getWritableDatabase();
        HomePlugsDB.delete(TABLE_PLUGS, KEY_ADDRESS + "= ?", new String[]{plug.getAddress()});
        HomePlugsDB.close();
    }

    /// ------------------------------------- ///

    /// -------------- PROGRAMs --------------- ///

    // Adding new program
    public void addProgram(Program program) {
        SQLiteDatabase HomePlugsDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, program.getPos());
        values.put(KEY_ADDRESS, program.getAddress());
        values.put(KEY_TIMEON, program.getTimeOn());
        values.put(KEY_TIMEOFF, program.getTimeOff());
        values.put(KEY_WEEK, program.getStringWeek());

        HomePlugsDB.insert(TABLE_PROGRAMS, null, values);
        HomePlugsDB.close();
    }

    // Getting single program
    public Program getProgram(Integer pos, String address) {
        SQLiteDatabase HomePlugsDB = getReadableDatabase();
        Cursor cursor = HomePlugsDB.query(
                TABLE_PROGRAMS,
                new String[]{KEY_ID, KEY_ADDRESS, KEY_TIMEON, KEY_TIMEOFF, KEY_WEEK},
                KEY_ADDRESS + "= ? AND " + KEY_ID + " = ?", new String[]{address, pos.toString()},
                null, null, null, null);

        if (cursor.getCount() == 1) {
            cursor.moveToFirst();

            Program new_program = new Program(
                    Integer.valueOf(cursor.getString(0)),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4));
            return new_program;
        }
        return null;
    }

    // Getting All programs
    public HashSet<Program> getAllPrograms(String address) {
        HashSet<Program> internal_hashset = new HashSet<>();
        SQLiteDatabase HomePlugsDB = getReadableDatabase();

        Cursor cursor = HomePlugsDB.query(
                TABLE_PROGRAMS,
                new String[]{KEY_ID, KEY_ADDRESS, KEY_TIMEON, KEY_TIMEOFF, KEY_WEEK},
                KEY_ADDRESS + "= ?",
                new String[]{address},
                null, null, KEY_ID + " ASC");

        if (cursor.moveToFirst()) {
            do {
                Program new_program = new Program(
                        Integer.valueOf(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4));
                internal_hashset.add(new_program);
            } while (cursor.moveToNext());
        }
        return internal_hashset;
    }

    // Updating single program
    public void updateProgram(Program program) {
        SQLiteDatabase HomePlugsDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, program.getPos());
        values.put(KEY_ADDRESS, program.getAddress());
        values.put(KEY_TIMEON, program.getTimeOn());
        values.put(KEY_TIMEOFF, program.getTimeOff());
        values.put(KEY_WEEK, program.getStringWeek());
        HomePlugsDB.update(TABLE_PROGRAMS, values, KEY_ID + " = ? AND " + KEY_ADDRESS + " = ?", new String[]{program.getPos().toString(), program.getAddress()});
        HomePlugsDB.close();
    }

    public void deleteProgram(Program program) {
        SQLiteDatabase HomePlugsDB = getWritableDatabase();
        HomePlugsDB.delete(TABLE_PROGRAMS, KEY_ID + " = ? AND " + KEY_ADDRESS + " = ?", new String[]{program.getPos().toString(), program.getAddress()});
        HomePlugsDB.close();
    }

    public int howManyPrograms(String address) {
        SQLiteDatabase HomePlugsDB = getReadableDatabase();

        Cursor cursor = HomePlugsDB.query(
                TABLE_PROGRAMS,
                new String[]{KEY_ID, KEY_ADDRESS, KEY_TIMEON, KEY_TIMEOFF, KEY_WEEK},
                KEY_ADDRESS + "= ?",
                new String[]{address},
                null, null, KEY_ID + " ASC");

        if (cursor.moveToFirst()) {
            return cursor.getCount();
        }
        return 0;
    }

    public int howManyEnabledPrograms(String address) {
        HashSet<Program> internal_hashset = new HashSet<>();
        SQLiteDatabase HomePlugsDB = getReadableDatabase();
        int count = 0;

        Cursor cursor = HomePlugsDB.query(
                TABLE_PROGRAMS,
                new String[]{KEY_ID, KEY_ADDRESS, KEY_TIMEON, KEY_TIMEOFF, KEY_WEEK},
                KEY_ADDRESS + "= ?",
                new String[]{address},
                null, null, KEY_ID + " ASC");

        if (cursor.moveToFirst()) {
            do {
                Program new_program = new Program(
                        Integer.valueOf(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4));
                internal_hashset.add(new_program);
            } while (cursor.moveToNext());

            for (Program p : internal_hashset) {
                if (p.getState()) count++;
            }
        }
        return count;
    }

    /// ------------------------------------- ///
}
