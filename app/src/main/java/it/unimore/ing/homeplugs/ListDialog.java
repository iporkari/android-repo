package it.unimore.ing.homeplugs;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ListDialog extends DialogFragment {

    HomePlugsDatabaseHandler HomePlugsDB = HomePlugsDatabaseHandler.getInstance(this.getContext());
    BluetoothAdapter btAdapter;

    ListDialog.ListDialogListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> mySet = btAdapter.getBondedDevices();
        HashSet<Plug> myPlugs = HomePlugsDB.getAllPlugs();
        HashSet<BluetoothDevice> myHashSet = new HashSet<>();
        ArrayList<HashMap<String, String>> list_simpleadapter = new ArrayList<>();
        for (BluetoothDevice bd : mySet) {
            if (bd.getAddress().substring(0, 8).equals("20:16:06")) {
                myHashSet.add(bd);
            }
        }
        for (Plug p : myPlugs) {
            if (p.getFav() == true) {
                myHashSet.remove(btAdapter.getRemoteDevice(p.getAddress()));
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (myHashSet.isEmpty() == false) {
            for (BluetoothDevice bd : myHashSet) {
                HashMap<String, String> temp_hash = new HashMap<>();
                temp_hash.put("name", bd.getName());
                temp_hash.put("address", bd.getAddress());
                list_simpleadapter.add(temp_hash);
            }

            final SimpleAdapter myAdapter = new SimpleAdapter(this.getContext(), list_simpleadapter, R.layout.plug_row_list, new String[]{"name", "address"}, new int[]{R.id.textup, R.id.textdown});

            builder.setTitle(R.string.list_dialog_title)
                    .setAdapter(myAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mListener.onClick(dialog, which);
                        }
                    });
        } else {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.empty_pair, null));
        }
        return builder.create();
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //((ListView)getDialog().findViewById(R.id.listview_pair)).setEmptyView(getDialog().findViewById(R.id.empty_pairlayout));
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (ListDialog.ListDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public interface ListDialogListener {
        void onClick(DialogInterface dialog, int which);
    }

}
