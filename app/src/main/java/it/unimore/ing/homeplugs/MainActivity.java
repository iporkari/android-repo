package it.unimore.ing.homeplugs;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Switch;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements ListDialog.ListDialogListener {

    private BluetoothAdapter btAdapter;                                                             // adattatore bluetooth del dispositivo
    private HashMap<String, Boolean> isConnected;                                                    // array booleano di controllo del collegamento effettuato di una plug
    private HashMap<String, BluetoothSocket> btSocket;                                              // hashmap di socket bluetooth per il collegamento ad una plug
    private GregorianCalendar calendar;                                                             // data ed ora inviata ad arduino
    private Menu myMenu;                                                                            // menu toolbar
    private FloatingActionButton myButton;                                                          // bottone Floating di aggiunta dispositivi
    protected final static UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");   // uuid identificativo del dispositivo arduino
    public static String MAC_ADDRESS = "device_address";                                            // tag del campo extra di apertura activity di programmazione
    final static private int max_plug = 50;                                                         // massime prese supportate
    private ListView lv_fav;                                                                        // list view dispositivi preferiti
    private boolean alarmMode = false;                                                              // flag di attivazione/deattivazione modalità allarme

    HomePlugsDatabaseHandler HomePlugsDB = HomePlugsDatabaseHandler.getInstance(this);              // database

    public String addressDialog = new String();
    public int happened;

    class ConnectTask extends AsyncTask<View, Void, View> {

        boolean running;
        ProgressDialog progressDialog;
        String address;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this, null, "Waiting...", true, false);
            running = true;
            address = addressDialog;
        }

        @Override
        protected View doInBackground(View... views) {
            View view = views[0];
            while (running) {
                Plug plug = HomePlugsDB.getPlug(address);

                // se la plug non è connessa (ovvero la btsocket è nulla), apro quest'ultima e aggiorno il db
                if (btSocket.get(address) == null) {
                    try {
                        btSocket.put(address, btAdapter.getRemoteDevice(plug.getAddress()).createInsecureRfcommSocketToServiceRecord(myUUID));
                        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                        btSocket.get(address).connect();
                        msg("Connected to " + plug.getName());
                        isConnected.put(address, true);
                        happened = R.color.colorConnected;
                        if ((btSocket.get(address) != null) && isConnected.get(address) == true) {
                            String date = "TIME_" + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) +
                                    ":" + calendar.get(Calendar.SECOND) + "_" + calendar.get(Calendar.DAY_OF_MONTH) + "_" +
                                    calendar.get(Calendar.MONTH) + "_" + calendar.get(Calendar.YEAR);
                            btSocket.get(address).getOutputStream().write(date.toString().getBytes());
                        }
                    } catch (Exception e) {
                        msg("Error connecting to plug " + plug.getName());
                        isConnected.put(address, false);
                        btSocket.remove(address);
                        happened = R.color.colorUndefined;
                        running = false;
                    }
                    HomePlugsDB.updatePlug(plug);
                    plug = HomePlugsDB.getPlug(address);
                }
                // essendo già eventualmente aperta la socket e la plug connessa, accendo e spengo!
                if ((btSocket.get(address) != null) && isConnected.get(address) == true) {
                    try {
                        if (plug.getState().equalsIgnoreCase("false")) {
                            btSocket.get(address).getOutputStream().write("ON".toString().getBytes());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                happened = R.color.colorActive;
                            }
                            plug.stateOn();
                        } else if (plug.getState().equalsIgnoreCase("true")) {
                            btSocket.get(address).getOutputStream().write("OFF".toString().getBytes());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                happened = R.color.colorInactive;
                            }
                            plug.stateOff();
                        }
                        HomePlugsDB.updatePlug(plug);
                    } catch (IOException e) {
                        msg("Error changing plug state");
                        running = false;
                    }
                }
            }
            return view;
        }

        @Override
        protected void onPostExecute(View view) {
            super.onPostExecute(view);
            progressDialog.dismiss();
            addressDialog = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setBackgroundColor(getResources().getColor(happened, getTheme()));
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // chiamata al metodo di inizializzazione
        init();

        // controllo compatibilità del sistema con il bluetooth
        if (btAdapter == null) {
            //Snackbar.make(findViewById(R.id.activity_main), R.string.no_bluetooth, Snackbar.LENGTH_LONG).show();
            finish();
        } else {
            if (!btAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }

        myButton = (FloatingActionButton) findViewById(R.id.floatingActionButtonMain);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNoticeListDialog();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    // metodo di inizializzazione dell'activity
    private void init() {
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        lv_fav = (ListView) findViewById(android.R.id.list);

        btSocket = new HashMap<>();
        isConnected = new HashMap<>();
        calendar = new GregorianCalendar();

        lv_fav.setEmptyView(findViewById(android.R.id.empty));
    }

    // creazione menu a scomparsa toolbar
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hp_menu, menu);
        myMenu = menu;
        return true;
    }

    // gestore menù
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings: {
                BTsetting();
                return true;
            }
            /*case R.id.siren: {
                alarm();
                return true;
            }*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // apertura dialog di aggiunta dispositivi associabili
    private void showNoticeListDialog() {
        DialogFragment dialog = new ListDialog();
        dialog.show(getSupportFragmentManager(), "ListDialogFragment");
    }

    // metodo di compilazione della lista dei dispositivi salvati
    private void CompileFavList() {
        // recupero tutte le plugs dal db
        HashSet<Plug> AllFavPlugs = new HashSet<Plug>(HomePlugsDB.getAllPlugs());
        // arraylist di appoggio per l'arrayadapter, ovvero per la listview
        ArrayList<HashMap<String, String>> list_simpleadapter = new ArrayList<>();
        // arraylist di appoggio per ordinare l'hashset AllFavPlug
        ArrayList<Plug> AllFavPlugsList = new ArrayList<>(AllFavPlugs);
        Collections.sort(AllFavPlugsList);
        // riempimento dell'arraylist di hasmap
        for (Plug p : AllFavPlugsList) {
            if (p.getFav() == true) {
                HashMap<String, String> temp_hash = new HashMap<>();
                temp_hash.put("name", p.getName());
                temp_hash.put("address", p.getAddress());
                list_simpleadapter.add(temp_hash);
            }
        }
        // implementazione simple adapter
        final myMainSimpleAdapter simpleAdapter = new myMainSimpleAdapter(this, list_simpleadapter, R.layout.plug_row_list, new String[]{"name", "address"}, new int[]{R.id.textup, R.id.textdown});
        // set dell'adapter e dei listener per la listview
        lv_fav.setAdapter(simpleAdapter);
        lv_fav.setOnItemClickListener(myListClickListener);
        lv_fav.setOnItemLongClickListener(myFavLongListClickListener);
    }

    // aggiornamento delle listview
    private void refresh() {
        CompileFavList();
    }

    // funzione di comparsa snackbar con stringa a piacere
    private void msg(String s) {
        Snackbar.make(findViewById(R.id.myCoordinator), s, Snackbar.LENGTH_LONG).show();
    }

    // metodo pulsante Bluetooth —> impostazioni di sistema
    private void BTsetting() {
        startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
    }

    private void alarm() {
        if (alarmMode == false) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                myMenu.getItem(0).setIcon(getResources().getDrawable(R.drawable.siren_on, this.getTheme()));
            }
            msg("Activating Alarm Mode...");
            alarmMode = true;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                myMenu.getItem(0).setIcon(getResources().getDrawable(R.drawable.siren_off, this.getTheme()));
            }
            msg("Deactivating Alarm Mode...");
            alarmMode = false;
        }
    }

    // interfaccia di gestione nella mainactivity della risposta al dialog di aggiunta dispositivi
    @Override
    public void onClick(DialogInterface dialog, int which) {
        ListView lw = ((AlertDialog) dialog).getListView();
        String address = ((HashMap<String, String>) lw.getAdapter().getItem(which)).get("address");
        Plug ctrl_plug = HomePlugsDB.getPlug(address);
        if (ctrl_plug != null) {
            ctrl_plug.favOn();
            HomePlugsDB.updatePlug(ctrl_plug);
        } else {
            Plug new_plug = new Plug(address, btAdapter.getRemoteDevice(address).getName(), "null", new Integer(1), new Integer(0));
            HomePlugsDB.addPlug(new_plug);
        }
        refresh();
    }

    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            // mi salvo le variabili importanti di posizione dentro la lista (position), l'indirizzo MAC del dispositivo (address) e la recupero dal DB la relativa Plug
            Integer position = new Integer(i);
            TextView textView = (TextView) view.findViewById(R.id.textdown);
            String info = textView.getText().toString();
            String address = info.substring(info.length() - 17);
            addressDialog = address;

            ConnectTask connectTask = new ConnectTask();
            connectTask.execute(view);

        }
    };

    // listener per invocare l'intent dell'activity programmazione
    private AdapterView.OnItemLongClickListener myFavLongListClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            TextView textView = (TextView) view.findViewById(R.id.textdown);
            String info = textView.getText().toString();
            String address = info.substring(info.length() - 17);
            Intent intent = new Intent(MainActivity.this, ProgramActivity.class);
            intent.putExtra(MAC_ADDRESS, address);
            startActivity(intent);
            return true;
        }
    };

    class myMainSimpleAdapter extends SimpleAdapter {

        public myMainSimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view.findViewById(R.id.textdown);
            String info = textView.getText().toString();
            String address = info.substring(info.length() - 17);
            Plug myListPlug = HomePlugsDB.getPlug(address);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (myListPlug.getState().equalsIgnoreCase("true")) {
                    view.setBackgroundColor(getResources().getColor(R.color.colorActive, getTheme()));
                }
                if (myListPlug.getState().equalsIgnoreCase("false")) {
                    view.setBackgroundColor(getResources().getColor(R.color.colorInactive, getTheme()));
                }
            }
            return view;
        }
    }

    public final static String controlNum (String number){
        if (number.length() == 1)
            return "0".concat(number);
        else
            return number;
    }
}










