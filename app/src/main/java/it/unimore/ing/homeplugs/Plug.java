package it.unimore.ing.homeplugs;

import java.io.Serializable;

public class Plug implements Serializable,Comparable<Plug> {

    private String name;
    private String address;
    private String state;
    private Boolean fav;
    private Boolean alarm;

    // --- COSTRUTTORI --- //
    // costruttore tipico
    public Plug(String address, String name) {
        this.name = name;
        this.address = address;
        this.state = "null";
        this.fav = false;
        this.alarm = false;
    }

    // costruttore null
    public Plug() {
        name = null;
        address = null;
        state = "null";
        fav = false;
        alarm = false;
    }

    // costruttore completo con Boolean [lato program]
    public Plug(String address, String name, String state, Boolean fav, Boolean alarm) {
        this.name = name;
        this.address = address;
        setState(state);
        setFav(fav);
        setAlarm(alarm);
    }

    // costruttore completo con Integer [lato sql]
    public Plug(String address, String name, String state, Integer fav, Integer alarm) {
        this.name = name;
        this.address = address;
        setState(state);
        setIntFav(fav);
        setIntAlarm(alarm);
    }

    // --------------------- //
    // NAME
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // ADDRESS
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    // STATE
    public void setState(String state) {
        this.state = state;
    }
    public String getState() {
        return state;
    }
    public String changeState() {
        if (state.equalsIgnoreCase("true")) {
            state = "false";
        }
        if (state.equalsIgnoreCase("false")){
            state = "true";
        }
        return getState();
    }
    public void setNullState() {
        this.state = "null";
    }
    public Boolean getBoolState(){
        if(this.state.equalsIgnoreCase("null")) return null;
        if (this.state.equalsIgnoreCase("true")) return true;
        else return false;
    }

    // FAV
    public Integer getIntFav() {
        if (fav == true) return new Integer(1);
        else return new Integer(0);
    }
    public void setIntFav(Integer fav) {
        if (fav.intValue() == 1) this.fav = true;
        else this.fav = false;
    }
    public Boolean getFav() {
        return fav;
    }
    public void setFav(boolean fav) {
        this.fav = fav;
    }

    // ALARM
    public Integer getIntAlarm() {
        if (alarm == true) return new Integer(1);
        else return new Integer(0);
    }
    public void setIntAlarm(Integer alarm) {
        if (alarm.intValue() == 1) this.alarm = true;
        else this.alarm = false;
    }
    public Boolean getAlarm() {
        return this.alarm;
    }
    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    // --------------------- //
    // --- METODI di ON --- //
    public void favOn() {
        this.fav = true;
    }
    public void alarmOn() {
        this.alarm = true;
    }
    public void stateOn() {
        this.state = "true";
    }


    // --- METODI di OFF --- //
    public void alarmOff() {
        this.alarm = false;
    }
    public void favOff() {
        this.fav = false;
    }
    public void stateOff() {
        this.state = "false";
    }

    // toString
    public String toString() {
        return new String("ADDRESS: " + address
                + " - NAME: " + name
                + " - STATE: " + state
                + " - FAV: " + fav
                + " - ALARM: " + alarm);
    }

    @Override
    public int compareTo(Plug plug) {
        return this.address.compareTo(plug.getAddress());
    }
}
