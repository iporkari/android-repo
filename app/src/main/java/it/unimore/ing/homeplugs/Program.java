package it.unimore.ing.homeplugs;

import android.util.Log;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Program implements Comparable<Program> {

    final static int STATE = 0;
    Integer pos;
    String address;
    Calendar TIME_ON;
    Calendar TIME_OFF;

    Boolean[] week;

    // week[0] = program state
    // week[Calendar.SUNDAY] = week[1]
    // week[Calendar.MONDAY] = week[2]
    // week[Calendar.TUESDAY] = week[3]
    // week[Calendar.WEDNESDAY] = week[4]
    // week[Calendar.THURSDAY] = week[5]
    // week[Calendar.FRIDAY] = week[6]
    // week[Calendar.SATURDAY] = week[7]

    // costruttore vuoto
    public Program() {
        pos = null;
        address = null;
        TIME_ON = null;
        TIME_OFF = null;
        week = new Boolean[]{false, false, false, false, false, false, false, false};
    }

    // costruttore generico
    public Program(String address) {
        this.pos = null;
        this.address = address;
        TIME_ON = null;
        TIME_OFF = null;
        week = new Boolean[]{false, false, false, false, false, false, false, false};
    }

    // costruttore completo con calendar [lato programma]
    public Program(Integer pos, String address, Calendar TIME_ON, Calendar TIME_OFF, Boolean[] week) {
        this.pos = pos;
        this.address = address;
        this.TIME_ON = TIME_ON;
        this.TIME_OFF = TIME_OFF;
        week = new Boolean[]{false, false, false, false, false, false, false, false};
        this.week = week;
    }

    // costruttore completo con string [lato sql]
    public Program(Integer pos, String address, String time_on, String time_off, String weekString) {
        setPos(pos);
        setAddress(address);
        TIME_ON = new GregorianCalendar();
        TIME_OFF = new GregorianCalendar();
        setTimeOn(time_on);
        setTimeOff(time_off);
        Log.d("weekString:",weekString);
        this.week = stringTOboolean(weekString);
    }

    // -- UTILITY -- //
    public static char booleanTOchar(Boolean bool) {
        if (bool == true) return 'Y';
        else return 'N';
    }

    public static Boolean scharTOboolean(String string) {
        if (string.equals("Y")) return true;
        else return false;
    }

    public static String booleanTOstring(Boolean[] array) {
        String string = new String();
        if (array.length == 8) {
            for (Boolean b : array) {
                string += booleanTOchar(b);
                string += ' ';
            }
            return string.substring(0, 15);
        } else return null;
    }

    public static Boolean[] stringTOboolean(String string) {
        Boolean[] array = new Boolean[8];
        if (string.length() == 15) {
            array[0] = scharTOboolean(string.substring(0, 1));
            array[1] = scharTOboolean(string.substring(2, 3));
            array[2] = scharTOboolean(string.substring(4, 5));
            array[3] = scharTOboolean(string.substring(6, 7));
            array[4] = scharTOboolean(string.substring(8, 9));
            array[5] = scharTOboolean(string.substring(10, 11));
            array[6] = scharTOboolean(string.substring(12, 13));
            array[7] = scharTOboolean(string.substring(14, 15));
            return array;
        } else return null;
    }

    public Integer getPos() {
        return pos;
    }

    // -- POS -- //
    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public Boolean getState() {
        return week[Program.STATE];
    }

    // -- STATE -- //
    public void setState(Boolean state) {
        week[0] = state;
    }

    public String getAddress() {
        return address;
    }

    public String getStringState () {
        if (week[Program.STATE] == true) return "Enabled";
        else return "Disabled";
    }

    // -- ADDRESS -- //
    public void setAddress(String address) {
        this.address = address;
    }

    public String getTimeOn() {
        String hour = String.valueOf(TIME_ON.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(TIME_ON.get(Calendar.MINUTE));
        hour = MainActivity.controlNum(hour);
        minute = MainActivity.controlNum(minute);
        return hour.concat(":").concat(minute);
    }

    // -- TIME_ON -- //
    public void setTimeOn(String timeOn) {
        TIME_ON.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeOn.substring(0, 2)));
        TIME_ON.set(Calendar.MINUTE, Integer.parseInt(timeOn.substring(3, 5)));
    }

    public Calendar getTIME_ON() {
        return TIME_ON;
    }

    public void setTIME_ON(Calendar TIME_ON) {
        this.TIME_ON = TIME_ON;
    }

    public String getTimeOff() {
        String hour = String.valueOf(TIME_OFF.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(TIME_OFF.get(Calendar.MINUTE));
        hour = MainActivity.controlNum(hour);
        minute = MainActivity.controlNum(minute);
        return hour.concat(":").concat(minute);
    }

    // -- TIME_OFF -- //
    public void setTimeOff(String timeOff) {
        TIME_OFF.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeOff.substring(0, 2)));
        TIME_OFF.set(Calendar.MINUTE, Integer.parseInt(timeOff.substring(3, 5)));
    }

    public Calendar getTIME_OFF() {
        return TIME_OFF;
    }

    public void setTIME_OFF(Calendar TIME_OFF) {
        this.TIME_OFF = TIME_OFF;
    }

    // -- WEEK -- //
    public Boolean[] getWeek() {
        return week;
    }

    public void setWeek(Boolean[] array) {
        if (array.length == 8)
            this.week = array;
    }

    public String getStringWeek() {
        return booleanTOstring(this.week);
    }

    public void setStringWeek(String stringWeek) {
        this.week = stringTOboolean(stringWeek);
    }

    public void setDayON(int index) {
        week[index] = true;
    }

    public void setDayOFF(int index) {
        week[index] = false;
    }

    public Boolean isRepeat(int index) {
        if (index >= Calendar.SUNDAY && index <= Calendar.SATURDAY)
            return week[index];
        else return null;
    }

    public String getStringRepeat() {
        if (week[Calendar.MONDAY] && week[Calendar.TUESDAY] && week[Calendar.WEDNESDAY] && week[Calendar.THURSDAY] && week[Calendar.FRIDAY] && week[Calendar.SATURDAY] && week[Calendar.SUNDAY])
            return "Repeat every day";
        if (week[Calendar.MONDAY] && week[Calendar.TUESDAY] && week[Calendar.WEDNESDAY] && week[Calendar.THURSDAY] && week[Calendar.FRIDAY] && !week[Calendar.SATURDAY] && !week[Calendar.SUNDAY])
            return "Repeat during weekdays";
        if (!week[Calendar.MONDAY] && !week[Calendar.TUESDAY] && !week[Calendar.WEDNESDAY] && !week[Calendar.THURSDAY] && !week[Calendar.FRIDAY] && week[Calendar.SATURDAY] && week[Calendar.SUNDAY])
            return "Repeat during weekend";
        if (!week[Calendar.MONDAY] && !week[Calendar.TUESDAY] && !week[Calendar.WEDNESDAY] && !week[Calendar.THURSDAY] && !week[Calendar.FRIDAY] && !week[Calendar.SATURDAY] && !week[Calendar.SUNDAY])
            return "No repeat";
        String returnString = new String();
        if (week[Calendar.MONDAY]) returnString += "Mon ";
        if (week[Calendar.TUESDAY]) returnString += "Tue ";
        if (week[Calendar.WEDNESDAY]) returnString += "Wed ";
        if (week[Calendar.THURSDAY]) returnString += "Thu ";
        if (week[Calendar.FRIDAY]) returnString += "Fri ";
        if (week[Calendar.SATURDAY]) returnString += "Sat ";
        if (week[Calendar.SUNDAY]) returnString += "Sun";
        return returnString;
    }

    @Override
    public String toString() {

        // typical toString return:
        // "_POS:nn+ON:hh:mm-OFF:hh:mm*WEEK:Y N Y N Y N Y" = 45chars
        //
        String stringPos = new String(pos.toString());
        stringPos = MainActivity.controlNum(stringPos);
        return "_POS:" + stringPos + "+ON:" + this.getTimeOn() + "-OFF:" + this.getTimeOff() + "*WEEK:"+  this.getStringWeek().substring(2,15) + "";
    }

    @Override
    public int compareTo(Program program) {
        if (this.pos.intValue() > program.getPos().intValue())
            return 1;
        if (this.pos.intValue() < program.getPos().intValue())
            return -1;
        return 0;
    }
}

