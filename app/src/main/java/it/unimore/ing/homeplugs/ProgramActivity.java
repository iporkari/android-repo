package it.unimore.ing.homeplugs;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ProgramActivity extends AppCompatActivity implements RenameDialog.RenameDialogListener, FABProgressListener, RemoveFavDialog.RemoveFavDialogListener {

    public static String NEW = "new_device";
    public static String POS = "position";
    public static String MAC = "mac_address";
    boolean alreadyDone;
    HomePlugsDatabaseHandler HomePlugsDB = HomePlugsDatabaseHandler.getInstance(this);
    private Toolbar myToolbar = null;
    private BluetoothAdapter btAdapter;
    private BluetoothSocket btSocket = null;
    private boolean isConnected = false;
    private Plug myPlug;
    private ListView lv_prog;
    private FloatingActionButton myButton;
    private FloatingActionButton mySaveButton;
    private FABProgressCircle myProgressCircle;
    private boolean running = false;

    // variabili per cambio button da opaco a colorato
    private Menu myMenu;

    //
    // chiamata al dialog di rinomina
    public void showRenameDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new RenameDialog();
        dialog.show(getSupportFragmentManager(), "RenameDialogFragment");
    }

    //
    // risposta al dialog di rinomina gestita dall'activity
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        EditText txt = (EditText) dialog.getDialog().findViewById(R.id.rename_edit);
        String name = txt.getText().toString();
        myPlug.setName(name);
        getSupportActionBar().setTitle(name);
        updatePlug();
    }

    // visualizzazione snackbar customizzata
    private void msg(String s) {
        Snackbar.make(findViewById(R.id.myCoordinator2), s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);
        Intent newint = getIntent();
        String address = newint.getStringExtra(MainActivity.MAC_ADDRESS);

        init(address);

        refresh();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        super.onNewIntent(intent);
    }

    // creazione menu a scomparsa toolbar
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hp_program_menu, menu);
        myMenu = menu;
        return true;
    }

    // gestore menù
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.siren: {
                if (myPlug.getAlarm() == true) {
                    myPlug.alarmOff();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        myMenu.getItem(0).setIcon(getResources().getDrawable(R.drawable.siren_off, this.getTheme()));
                    }
                    msg("Deleted from Alarm Mode List");
                } else {
                    myPlug.alarmOn();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        myMenu.getItem(0).setIcon(getResources().getDrawable(R.drawable.siren_on, this.getTheme()));
                    }
                    msg("Added to Alarm Mode List");
                }
                updatePlug();
                return true;
            }*/
            case R.id.rename_button: {
                showRenameDialog();
                return true;
            }

            case R.id.favorite: {           // elimino o aggiungo (ma attualmente solo elimino) dal db la presa e conseguentemente tutti i programmi
                showDeleteDialog();
                return true;
            }

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDeleteDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new RemoveFavDialog();
        dialog.show(getSupportFragmentManager(), "RemoveFavDialogFragment");
    }

    private void init(final String address) {
        // recuperiamo le variabili
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        Log.d(MAC, address);
        myPlug = HomePlugsDB.getPlug(address);
        lv_prog = (ListView) findViewById(R.id.list_prog);
        final Switch mySwitch = (Switch) findViewById(R.id.switch_button);

        // impostiamo la toolbar
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(myPlug.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // impostiamo la listview
        lv_prog.setEmptyView(findViewById(R.id.empty_prog));

        // impostiamo lo switch
        if (myPlug.getState().equalsIgnoreCase("true"))
            setColorActive(mySwitch);
        if (myPlug.getState().equalsIgnoreCase("false"))
            setColorInactive(mySwitch);
        if (myPlug.getState().equalsIgnoreCase("null"))
            dont(mySwitch);

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    SwitchON(compoundButton);
                } else {
                    if (!alreadyDone) SwitchOFF(compoundButton);
                    else setColorInactive(compoundButton);
                }
            }
        });

        myButton = (FloatingActionButton) findViewById(R.id.floatingActionButtonProgramAdd);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProgramActivity.this, TimeActivity.class);
                intent.putExtra(MAC, myPlug.getAddress());
                intent.putExtra(NEW, true);
                startActivity(intent);
            }
        });

        myProgressCircle = (FABProgressCircle) findViewById(R.id.fabProgressCircle);
        myProgressCircle.attachListener(this);

        mySaveButton = (FloatingActionButton) findViewById(R.id.floatingActionButtonProgramSave);
        mySaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!running) {
                    if (btSocket != null && isConnected == true) {
                        running = true;
                        myProgressCircle.show();
                        new CountDownTimer(1500, 1500) {
                            @Override
                            public void onTick(long l) {
                            }

                            @Override
                            public void onFinish() {
                                try {
                                    // recupero tutti i programmi relativi alla mia plug dal db
                                    HashSet<Program> AllPrograms = new HashSet<Program>(HomePlugsDB.getAllPrograms(myPlug.getAddress()));
                                    // arraylist di appoggio per ordinare l'hashset AllPrograms
                                    ArrayList<Program> AllProgramsList = new ArrayList<>(AllPrograms);
                                    Collections.sort(AllProgramsList);
                                    String howMany = String.valueOf(HomePlugsDB.howManyEnabledPrograms(myPlug.getAddress()));
                                    howMany = MainActivity.controlNum(howMany);
                                    btSocket.getOutputStream().write("PROG:".toString().getBytes());
                                    btSocket.getOutputStream().write(howMany.getBytes());
                                    for (Program p : AllProgramsList) {
                                        if (p.getState()) {
                                            btSocket.getOutputStream().write(p.toString().getBytes());
                                        }
                                    }
                                    myProgressCircle.beginFinalAnimation();
                                    running = false;
                                } catch (Exception e) {
                                    myProgressCircle.hide();
                                    msg("Error programming plug");
                                    running = false;
                                }
                            }
                        }.start();
                    } else {
                        msg("Plug " + myPlug.getName() + " is not connected");
                    }
                }
            }
        });
    }

    private void refresh() {
        CompileProgList();
    }

    private void CompileProgList() {
        // recupero tutti i programmi relativi alla mia plug dal db
        HashSet<Program> AllPrograms = new HashSet<Program>(HomePlugsDB.getAllPrograms(myPlug.getAddress()));
        // arraylist di appoggio per l'arrayadapter, ovvero per la listview
        ArrayList<HashMap<String, String>> list_simpleadapter = new ArrayList<>();
        // arraylist di appoggio per ordinare l'hashset AllPrograms
        ArrayList<Program> AllProgramsList = new ArrayList<>(AllPrograms);
        Collections.sort(AllProgramsList);
        // riempimento dell'arraylist di hasmap
        for (Program p : AllProgramsList) {
            {
                HashMap<String, String> temp_hash = new HashMap<>();
                temp_hash.put("time", p.getTimeOn() + " - " + p.getTimeOff());
                temp_hash.put("repeat", p.getStringRepeat());
                list_simpleadapter.add(temp_hash);
            }
        }
        // implementazione simple adapter
        final myProgramSimpleAdapter simpleAdapter = new myProgramSimpleAdapter(this, list_simpleadapter, R.layout.plug_row_list_with_switch, new String[]{"time", "repeat"}, new int[]{R.id.textup, R.id.textdown});
        // set dell'adapter e dei listener per la listview
        lv_prog.setAdapter(simpleAdapter);
        //lv_prog.setOnItemClickListener(myListClickListener);

    }

    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            Intent intent = new Intent(ProgramActivity.this, TimeActivity.class);
            intent.putExtra(MAC, myPlug.getAddress());
            intent.putExtra(NEW, false);
            intent.putExtra(POS, i);
            startActivity(intent);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void connect(CompoundButton mySwitch) {
        ConnectTask connectTask = new ConnectTask();
        connectTask.execute(mySwitch);
    }

    private void updatePlug() {
        HomePlugsDB.updatePlug(myPlug);
        myPlug = HomePlugsDB.getPlug(myPlug.getAddress());
    }

    private void SwitchON(CompoundButton mySwitch) {
        connect(mySwitch);
        if (btSocket != null && isConnected == true) {
            try {
                btSocket.getOutputStream().write("ON".toString().getBytes());
                setColorActive(mySwitch);
                myPlug.setState("true");
                updatePlug();
            } catch (IOException e) {
                msg("Error changing plug state");
                dont(mySwitch);
            }
        }
    }

    private void SwitchOFF(CompoundButton mySwitch) {
        connect(mySwitch);
        if (btSocket != null && isConnected == true) {
            try {
                btSocket.getOutputStream().write("OFF".toString().getBytes());
                setColorInactive(mySwitch);
                myPlug.setState("false");
                updatePlug();
            } catch (IOException e) {
                msg("Error changing plug state");
                dont(mySwitch);
            }
        }
    }

    private void dont(CompoundButton mySwitch) {
        alreadyDone = true;
        mySwitch.setChecked(false);
        setColorUndefined(mySwitch);
        isConnected = false;
        btSocket = null;
        alreadyDone = false;
    }

    private void setColorActive(CompoundButton mySwitch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mySwitch.setText("Active");
            mySwitch.setBackgroundColor(getResources().getColor(R.color.colorActive, getTheme()));
            mySwitch.setTextColor(getResources().getColor(R.color.colorActiveText, getTheme()));
        }
    }

    private void setColorInactive(CompoundButton mySwitch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mySwitch.setText("Inactive");
            mySwitch.setBackgroundColor(getResources().getColor(R.color.colorInactive, getTheme()));
            mySwitch.setTextColor(Color.WHITE);
        }
    }

    private void setColorUndefined(CompoundButton mySwitch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mySwitch.setChecked(false);
            mySwitch.setText("Undefined");
            mySwitch.setBackgroundColor(getResources().getColor(R.color.colorUndefined, getTheme()));
            mySwitch.setTextColor(Color.WHITE);
        }
    }

    @Override
    public void onFABProgressAnimationEnd() {
    }

    @Override
    public void onRemoveFavPositiveClick(DialogFragment dialog) {
        // recupero tutti i programmi relativi alla mia plug dal db
        HashSet<Program> AllPrograms = new HashSet<Program>(HomePlugsDB.getAllPrograms(myPlug.getAddress()));
        for (Program p : AllPrograms) {
            HomePlugsDB.deleteProgram(p);
        }
        HomePlugsDB.deletePlug(myPlug);
        NavUtils.navigateUpFromSameTask(this);
    }

    class ConnectTask extends AsyncTask<CompoundButton, Void, CompoundButton> {

        boolean catchLaunched;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ProgramActivity.this, null, "Waiting...", true, false);
            catchLaunched = false;
        }

        @Override
        protected CompoundButton doInBackground(CompoundButton... mySwitch) {

            if (btSocket == null) {
                try {
                    btSocket = btAdapter.getRemoteDevice(myPlug.getAddress()).createInsecureRfcommSocketToServiceRecord(MainActivity.myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();
                    msg("Connected to " + myPlug.getName());
                    isConnected = true;
                    catchLaunched = false;
                } catch (Exception e) {
                    msg("Error connecting to plug " + myPlug.getName());
                    catchLaunched = true;
                }

            }
            return mySwitch[0];
        }

        @Override
        protected void onPostExecute(CompoundButton mySwitch) {
            super.onPostExecute(mySwitch);
            progressDialog.dismiss();
            if (catchLaunched) {
                dont(mySwitch);
            }
        }
    }

    class myProgramSimpleAdapter extends SimpleAdapter {

        public myProgramSimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            final Program myListProgram = HomePlugsDB.getProgram(position, myPlug.getAddress());

            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.plug_row_list_with_switch_clickable);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ProgramActivity.this, TimeActivity.class);
                    intent.putExtra(MAC, myPlug.getAddress());
                    intent.putExtra(NEW, false);
                    intent.putExtra(POS, position);
                    startActivity(intent);
                }
            });

            final Switch myListSwitch = (Switch) view.findViewById(R.id.switch_program_list);
            myListSwitch.setChecked(myListProgram.getState());

            myListSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        myListProgram.setState(true);
                    } else {
                        myListProgram.setState(false);
                    }
                    HomePlugsDB.updateProgram(myListProgram);
                }
            });

            return view;
        }
    }
}
