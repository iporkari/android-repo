package it.unimore.ing.homeplugs;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeActivity extends AppCompatActivity implements DeleteDialog.DeleteDialogListener {

    HomePlugsDatabaseHandler HomePlugsDB = HomePlugsDatabaseHandler.getInstance(this);

    private Toolbar myToolbar;
    private Plug myPlug;
    private Program myProgram;
    private FloatingActionButton myButton;
    private Boolean new_program;
    private GregorianCalendar startTime;
    private GregorianCalendar endTime;
    private TextView startTimeText;
    private TextView endTimeText;
    private SimpleDateFormat simpleDateFormat;
    private Boolean wrongTime;
    private String address;
    private Switch mySwitch;
    private int myPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mySwitch = (Switch) findViewById(R.id.switch_button_time);
        simpleDateFormat = new SimpleDateFormat("H:mm");
        startTime = new GregorianCalendar();
        endTime = new GregorianCalendar();
        address = new String();
        wrongTime = false;

        loadActivity();

        // impostiamo la toolbar
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(myPlug.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setStartTime();
        setEndTime();

        updateStartText();
        updateEndText();

        setSwitch();

    }

    private void loadActivity() {
        // boolean NEW = creazione di una programmazione nuova (true) | instanziazione di una programmazione esistente per modifica (false)
        // int POS = posizione nel db del program (da leggere solo in caso di NEW = false)
        // string ADDRESS = indirizzo MAC della presa nel db

        Intent newint = getIntent();
        address = newint.getStringExtra(ProgramActivity.MAC);
        Log.d(ProgramActivity.MAC, address);
        new_program = new Boolean(newint.getBooleanExtra(ProgramActivity.NEW, true));
        Log.d(ProgramActivity.NEW, new_program.toString());

        // metti la visualizzazione di default
        if (new_program == true) {
            Log.d("i'm:", "inside");
            endTime.set(Calendar.HOUR_OF_DAY, endTime.get(Calendar.HOUR_OF_DAY) + 1);
            myPos = HomePlugsDB.howManyPrograms(address);
            myProgram = new Program(myPos, address, startTime, endTime, new Boolean[]{true, false, false, false, false, false, false, false});
        }

        // carica dal db il program corrispondente
        if (new_program == false) {
            Integer pos = newint.getIntExtra(ProgramActivity.POS, -1);
            Log.d("sono il numero: ", pos.toString());
            myProgram = HomePlugsDB.getProgram(pos, address);
            startTime.set(Calendar.HOUR_OF_DAY, myProgram.getTIME_ON().get(Calendar.HOUR_OF_DAY));
            startTime.set(Calendar.MINUTE, myProgram.getTIME_ON().get(Calendar.MINUTE));
            endTime.set(Calendar.HOUR_OF_DAY, myProgram.getTIME_OFF().get(Calendar.HOUR_OF_DAY));
            endTime.set(Calendar.MINUTE, myProgram.getTIME_OFF().get(Calendar.MINUTE));

            setProgramCheckBox();

        }

        myPlug = HomePlugsDB.getPlug(address);
    }

    private void setProgramCheckBox() {
        CheckBox myCheckBox = (CheckBox) findViewById(R.id.check_mon);
        boolean myStateCheckBox = myProgram.isRepeat(Calendar.MONDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
        myCheckBox = (CheckBox) findViewById(R.id.check_tue);
        myStateCheckBox = myProgram.isRepeat(Calendar.TUESDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
        myCheckBox = (CheckBox) findViewById(R.id.check_wed);
        myStateCheckBox = myProgram.isRepeat(Calendar.WEDNESDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
        myCheckBox = (CheckBox) findViewById(R.id.check_thu);
        myStateCheckBox = myProgram.isRepeat(Calendar.THURSDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
        myCheckBox = (CheckBox) findViewById(R.id.check_fri);
        myStateCheckBox = myProgram.isRepeat(Calendar.FRIDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
        myCheckBox = (CheckBox) findViewById(R.id.check_sat);
        myStateCheckBox = myProgram.isRepeat(Calendar.SATURDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
        myCheckBox = (CheckBox) findViewById(R.id.check_sun);
        myStateCheckBox = myProgram.isRepeat(Calendar.SUNDAY);
        myCheckBox.setChecked(myStateCheckBox);
        if (myStateCheckBox) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        }
    }

    private void setSwitch() {

        mySwitch.setChecked(myProgram.getState());
        if (myProgram.getState() == true) setColorActive(mySwitch);
        if (myProgram.getState() == false) setColorInactive(mySwitch);

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Log.d("i'm:", "inside listener");
                if (isChecked) {
                    Log.d("isChecked", String.valueOf(isChecked));
                    setColorActive(compoundButton);
                    myProgram.setState(true);
                    //updateProgram();
                } else {
                    Log.d("isChecked", String.valueOf(isChecked));
                    setColorInactive(compoundButton);
                    myProgram.setState(false);
                }
            }
        });
    }

    private void setStartTime() {
        // ----------------------- START TIME ----------------------------- //
        // textview
        startTimeText = (TextView) findViewById(R.id.start_time);

        // listener del time picker dialog
        final TimePickerDialog.OnTimeSetListener startListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                startTime.set(Calendar.HOUR_OF_DAY, hour);
                startTime.set(Calendar.MINUTE, minute);
                updateStartText();
            }
        };

        // listener della textview
        startTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(
                        TimeActivity.this,
                        startListener,
                        startTime.get(Calendar.HOUR_OF_DAY),
                        startTime.get(Calendar.MINUTE),
                        true).show();
            }
        });
    }

    private void setEndTime() {
        // ----------------------- END TIME ----------------------------- //
        // textview
        endTimeText = (TextView) findViewById(R.id.end_time);

        // listener del time picker dialog
        final TimePickerDialog.OnTimeSetListener endListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                endTime.set(Calendar.HOUR_OF_DAY, hour);
                endTime.set(Calendar.MINUTE, minute);
                updateEndText();
            }
        };

        // listener della textview
        endTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(
                        TimeActivity.this,
                        endListener,
                        endTime.get(Calendar.HOUR_OF_DAY),
                        endTime.get(Calendar.MINUTE),
                        true).show();
            }
        });
    }

    private void updateStartText() {
        controlTime(startTimeText);
        startTimeText.setText(simpleDateFormat.format(startTime.getTime(), new StringBuffer(), new FieldPosition(DateFormat.MEDIUM)));
    }

    private void updateEndText() {
        controlTime(endTimeText);
        endTimeText.setText(simpleDateFormat.format(endTime.getTime(), new StringBuffer(), new FieldPosition(DateFormat.MEDIUM)));
    }

    private void controlTime(TextView field) {
        if (startTime.get(Calendar.HOUR_OF_DAY) > endTime.get(Calendar.HOUR_OF_DAY)) {
            wrongTime = true;
        }
        else {
            wrongTime = false;
        }
        if (startTime.get(Calendar.HOUR_OF_DAY) == endTime.get(Calendar.HOUR_OF_DAY) && startTime.get(Calendar.MINUTE) >= endTime.get(Calendar.MINUTE)) {
            wrongTime = true;
        }
        if (wrongTime) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                field.setTextColor(getResources().getColor(R.color.colorAccent, getTheme()));
            }
        } else {
            wrongTime = false;
            startTimeText.setTextColor(Color.DKGRAY);
            endTimeText.setTextColor(Color.DKGRAY);
            myProgram.setTIME_ON(startTime);
            myProgram.setTIME_OFF(endTime);
        }

        if (field == startTimeText && wrongTime)
            msg("Start time must be lesser than end time");
        if (field == endTimeText && wrongTime)
            msg("End time must be greater than end time");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.time_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bin: {
                showDeleteDialog();
                return true;
            }

            case android.R.id.home: {
                NavUtils.navigateUpFromSameTask(this);
                return true;
            }
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private void setColorActive(CompoundButton mySwitch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mySwitch.setText("Program Enabled");
            mySwitch.setBackgroundColor(getResources().getColor(R.color.colorActive, getTheme()));
            mySwitch.setTextColor(getResources().getColor(R.color.colorActiveText, getTheme()));
        }
    }

    private void setColorInactive(CompoundButton mySwitch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mySwitch.setText("Program Disabled");
            mySwitch.setBackgroundColor(getResources().getColor(R.color.colorInactive, getTheme()));
            mySwitch.setTextColor(Color.WHITE);
        }
    }

    private void msg(String s) {
        Snackbar.make(findViewById(R.id.myCoordinator3), s, Snackbar.LENGTH_LONG).show();
    }

    public void showDeleteDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new DeleteDialog();
        dialog.show(getSupportFragmentManager(), "DeleteDialogFragment");
    }

    @Override
    public void onDeletePositiveClick(DialogFragment dialog) {
        // mettere l'if (c'è nel db) cancellalo e in tutti i modi esci dall'activity
        if (new_program == false) HomePlugsDB.deleteProgram(myProgram);
        NavUtils.navigateUpFromSameTask(this);
    }

    public void onCheckDayClicked(View view) {
        //verifica se un bottone è checkato o no
        boolean checked = ((CheckBox) view).isChecked();
        CheckBox myCheckBox = (CheckBox) findViewById(view.getId());
        if (checked) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                myCheckBox.setTextColor(getResources().getColor(R.color.colorBackground, getTheme()));
            }
        } else myCheckBox.setTextColor(Color.BLACK);

        //controllo se i checkbox senon selezionati
        switch (view.getId()) {
            case R.id.check_mon: {
                if (checked) {
                    myProgram.setDayON(Calendar.MONDAY);
                } else {
                    myProgram.setDayOFF(Calendar.MONDAY);
                }
                break;
            }

            case R.id.check_tue: {
                if (checked) {
                    myProgram.setDayON(Calendar.TUESDAY);
                } else {
                    myProgram.setDayOFF(Calendar.TUESDAY);
                }
                break;
            }

            case R.id.check_wed: {
                if (checked) {
                    myProgram.setDayON(Calendar.WEDNESDAY);
                } else {
                    myProgram.setDayOFF(Calendar.WEDNESDAY);
                }
                break;
            }

            case R.id.check_thu: {
                if (checked) {
                    myProgram.setDayON(Calendar.THURSDAY);
                } else {
                    myProgram.setDayOFF(Calendar.THURSDAY);
                }
                break;
            }
            case R.id.check_fri: {
                if (checked) {
                    myProgram.setDayON(Calendar.FRIDAY);
                } else {
                    myProgram.setDayOFF(Calendar.FRIDAY);
                }
                break;
            }
            case R.id.check_sat: {
                if (checked) {
                    myProgram.setDayON(Calendar.SATURDAY);
                } else {
                    myProgram.setDayOFF(Calendar.SATURDAY);
                }
                break;
            }
            case R.id.check_sun: {
                if (checked) {
                    myProgram.setDayON(Calendar.SUNDAY);
                } else {
                    myProgram.setDayOFF(Calendar.SUNDAY);
                }
                break;
            }

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (new_program == true && !wrongTime) HomePlugsDB.addProgram(myProgram);
        if (new_program == false && !wrongTime) HomePlugsDB.updateProgram(myProgram);
    }
}

